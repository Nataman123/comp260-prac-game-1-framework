﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    public float speed = 4.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // get the vector from the bee to the target

        Vector2 direction;
        Vector2 targ1 = target1.position - transform.position;
        Vector2 targ2 = target2.position - transform.position;
        if (targ1.magnitude < targ2.magnitude)
        {
            direction = targ1;
        }
        else
        {
            direction = targ2;
        }

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
       
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;

        Vector2 direction;
        Vector2 targ1 = target1.position - transform.position;
        Vector2 targ2 = target2.position - transform.position;
        if (targ1.magnitude > targ2.magnitude)
        {
            direction = targ1;
        }
        else
        {
            direction = targ2;
        }
       
        Gizmos.DrawRay(transform.position, direction);
    }
}
