﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public float acceleration = 10.0f;
	public float brake = 5.0f;
	private float speed = 0.0f;
	public float turnSpeed = 1.0f;
	private float maxTurnSpeed = 30.0f;
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float turn = Input.GetAxis("Horizontal");

		// turn the car
		transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (brake > maxSpeed || brake < 0) {
				brake = maxSpeed;
			}
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
				if (speed < 0) {
					speed = 0;
				}
			}
				if (speed < 0) {
				speed = speed + brake * Time.deltaTime;
				if (speed > 0) {
					speed = 0;
				}
			} 
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
